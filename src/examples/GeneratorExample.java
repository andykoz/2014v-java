import java.util.Random;
import java.util.Scanner;

public class GeneratorExample {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int length = sc.nextInt();

        int[][] array = generateArray1(length);
        println(array);
    }

    final private static String BLANK = " ";

    private static void println(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            println(array[i]);
        }
    }

    private static void println(int[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            System.out.print(array[i] + BLANK);
        }
        System.out.println(array[array.length - 1]);
    }

    final private static int RANDOM_MAX_VALUE = 10;

    private static int[][] generateArray1(int length) {
        int[][] result = new int[length][];

        for (int i = 0; i < result.length; i++) {
            result[i] = generateArray2(length);
        }

        return result;
    }

    private static int[] generateArray2(int length) {
        int[] result = new int[length];
        Random random = new Random();

        for (int i = 0; i < result.length; i++) {
            result[i] = random.nextInt(RANDOM_MAX_VALUE);
        }

        return result;
    }


}
