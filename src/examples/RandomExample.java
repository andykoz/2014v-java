import java.util.Random;

public class RandomExample {

    public static void main(String[] args) {
        Random random = new Random();

        System.out.println(random.nextInt()); // Случайное целое из диапазона [-2^31; 2^31).
        System.out.println(random.nextInt(100)); // Случайное целое число из диапазона [0, 100).
    }
}
