package swingExamples;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Пример использования укладчиков и области для ввода текста.
 *
 * @author adkozlov
 */
public class Layouts extends JFrame {

    public static void main(String[] args) {
        new Layouts();
    }

    public static final String FRAME_TITLE = "Layouts example";

    public Layouts() {
        super(FRAME_TITLE);

        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(2, 1));

        final JTextArea textArea = new JTextArea();
        panel.add(textArea);

        JPanel upPanel = new JPanel();
        upPanel.setLayout(new GridLayout(2, 2));
        panel.add(upPanel);

        for (int i = 0; i < 4; i++) {
            final JButton button = new JButton();
            upPanel.add(button);

            button.setText((i + 1) + "");
            button.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    String text = button.getText();

                    textArea.append(text);
                }
            });
        }

        setContentPane(panel);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setVisible(true);
    }
}
