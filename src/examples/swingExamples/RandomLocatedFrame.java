package swingExamples;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Random;

/**
 * @author adkozlov
 */
public class RandomLocatedFrame extends JFrame {

    public static void main(String[] args) {
        new RandomLocatedFrame();
    }

    public static final String FRAME_TITLE = "Click the button";
    public static final String DEFAULT_BUTTON_TEXT = "Click me!";
    public static final String CLICKED_BUTTON_TEXT = "You're awesome!";
    public static final Dimension DEFAULT_DIMENSION = new Dimension(150, 150);

    public RandomLocatedFrame() {
        super(FRAME_TITLE);

        final JButton button = new JButton();
        button.setText(DEFAULT_BUTTON_TEXT);

        final Dimension screenResolution = Toolkit.getDefaultToolkit().getScreenSize();
        final Random random = new Random();
        button.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                setLocation(random.nextInt(screenResolution.width - getWidth()), random.nextInt(screenResolution.height - getHeight()));
            }
        });

        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                button.setText(CLICKED_BUTTON_TEXT);
            }
        });

        getContentPane().add(button);

        setSize(DEFAULT_DIMENSION);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
