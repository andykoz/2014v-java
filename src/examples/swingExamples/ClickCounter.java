package swingExamples;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Пример графического приложения, считающего количество нажатий на кнопку.
 *
 * @author adkozlov
 */
public class ClickCounter extends JFrame {

    public static void main(String[] args) {
        new ClickCounter();
    }

    public static final String FRAME_TITLE = "Click counter";
    public static final String BUTTON_DEFAULT_TEXT = "Click me!";

    public ClickCounter() {
        super(FRAME_TITLE);

        final JButton button = new JButton();
        button.setText(BUTTON_DEFAULT_TEXT);
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String text = button.getText();

                int c = (text.equals(BUTTON_DEFAULT_TEXT) ? 0 : Integer.parseInt(text)) + 1;
                button.setText(Integer.toString(c));
            }
        });

        getContentPane().add(button);

        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
    }
}
