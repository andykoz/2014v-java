package swingExamples;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * @author adkozlov
 */
public class Tabs extends JFrame {

    public static void main(String[] args) {
        new Tabs();
    }

    private Box createBox() {
        JButton b1 = new JButton("Open");

        Box verticalBox = Box.createVerticalBox();

        verticalBox.add(b1);
        verticalBox.add(Box.createVerticalStrut(10));

        final JTextArea textArea = new JTextArea();

        b1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser opener = new JFileChooser();

                if (opener.showOpenDialog(getParent()) == JFileChooser.APPROVE_OPTION) {
                    try {
                        Scanner scanner = new Scanner(opener.getSelectedFile());

                        textArea.setText("");
                        while (scanner.hasNext()) {
                            textArea.append(scanner.nextLine());

                            if (scanner.hasNext()) {
                                textArea.append("\n");
                            }
                        }

                        scanner.close();
                    } catch (FileNotFoundException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });

        Box box = Box.createHorizontalBox();
        box.add(verticalBox);
        box.add(new JScrollPane(textArea));

        return box;
    }

    public Tabs() {
        super("Text editor");

        JTabbedPane tabbedPane = new JTabbedPane();
        tabbedPane.add("1", createBox());
        setContentPane(tabbedPane);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        setSize(400, 400);
    }
}