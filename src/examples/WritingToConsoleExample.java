public class WritingToConsoleExample {

    public static void main(String[] args) {
        System.out.println(10); // Вывести целое число 10 и перевести строку.
        System.out.println('a'); // Вывести символ 'a'.

        System.out.print("Hello" + " "); // Вывести слово "Hello" и пробел без перевода строки.
        System.out.print("world"); // Вывести слово "world" и перевести строку.

        System.out.println(); // Перевод строки.
    }
}
