import java.util.Scanner;

public class ReadingFromConsoleExample2 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int i = sc.nextInt();
        double d = sc.nextDouble();

        System.out.println(i);
        System.out.println(d);
        System.out.print(i < d);
    }
}
