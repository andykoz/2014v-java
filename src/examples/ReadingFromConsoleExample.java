import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ReadingFromConsoleExample {

    public static void main(String[] args) throws IOException {
        int n = nextInt(); // Считать с консоли число n. Если ввести не число, программа "упадет".

        System.out.println(n); // Вывести число n на консоль.
    }

    private static int nextInt() throws IOException {
        return Integer.parseInt(bf.readLine());
    }

    private static double nextDouble() throws IOException {
        return Double.parseDouble(bf.readLine());
    }

    private static BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
}
