import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

/*
Программа выводит сумму двух целых чисел, записанных в файле sumin.txt, в файл sumout.txt.
 */
public class FileIOExample {

    public static void main(String[] args) throws FileNotFoundException {
        Scanner in = new Scanner(new File("sumin.txt"));
        int a = in.nextInt();
        int b = in.nextInt();
        in.close();

        PrintWriter out = new PrintWriter(new File("sumout.txt"));
        out.println(a + b);
        out.close();
    }
}
