public class IfExample {

    public static void main(String[] args) {
        System.out.println(abs(1.7));
        System.out.println(abs(-100));
    }

    /**
     * Вычисляет модуль числа согласно математическому определению.
     */
    static double abs(double x) {
        if (x >= 0) {
            return x;
        } else {
            return -x;
        }
    }
}
