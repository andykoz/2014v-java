public class ForExample {

    public static void main(String[] args) {
        int n = 10; // Значение числа n можно менять вручную.

        int sum = 0;
        for (int i = 0; i <= n; i++) {
            sum += i;
        }

        System.out.println(sum); // Результат, вычисленный сложением чисел от 0 до n.
        System.out.println(n * (n + 1) / 2); // Результат, вычисленный по формуле.
    }
}
