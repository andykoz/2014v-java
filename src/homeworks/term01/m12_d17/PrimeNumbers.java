package term01.m12_d17;

import java.util.Scanner;

/*
Требуется реализовать решето Эратосфена от 1 до n: sieve[i] == true, если число i + 1 простое. Число 1 считать простым.
Тело функции main изменять запрещено.
 */

public class PrimeNumbers {

    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        boolean[] isPrime = sieveOfEratosthenes(sc.nextInt());

        for (int i = 0; i < isPrime.length; i++) {
            if (isPrime[i]) {
                System.out.print((i + 1) + " ");
            }
        }
    }

    private static boolean[] sieveOfEratosthenes(int n) {
        boolean[] sieve = new boolean[n];

        return sieve;
    }
}
