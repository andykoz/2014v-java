package term01.m12_d17;

import java.util.Scanner;

/*
С консоли вводится n чисел. Требуется проверить, что массив является отсортированным по неубыванию.
Тело функции main изменять запрещено.
 */

public class IsSort {

    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        long[] arr = new long[sc.nextInt()];

        for (int i = 0; i < arr.length; i++) {
            arr[i] = sc.nextLong();
        }

        System.out.print(isSort(arr));
    }

    private static boolean isSort(long[] arr) {
        return false;
    }
}
