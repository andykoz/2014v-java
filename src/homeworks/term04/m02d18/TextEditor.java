package term04.m02d18;

import javax.swing.*;

/**
 * Требуется реализовать текстовый редактор, поддерживающий следующие операции:
 * 1) открыть файл;
 * 2) сохранить файл;
 * 3) выйти из программы;
 * 4) многовкладочный режим.
 * <p/>
 * Бонусы:
 * 1) название файла в имени вкладки;
 * 2) звездочка в названии файла, если файл не сохранен;
 * 3) запрос на сохранение при закрытии вкладки;
 * 4) загрузка только части текста.
 *
 * @author adkozlov
 */
public class TextEditor extends JFrame {

    public static void main(String[] args) {
        new TextEditor();
    }

    public TextEditor() {
        super("Notepad");
    }
}
