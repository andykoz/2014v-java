package term04.m02d04;

/**
 * Требуется реализовать графическое приложение.
 * При попытке навести курсор на кнопку приложение случайно изменяет свое местоположение.
 * <p/>
 * В качестве примера графического приложения следует использовать класс ClickCounter.
 * <p/>
 * установка позиции окна:
 *
 * @author adkozlov
 * @see java.awt.Window#setLocation(int, int)
 * <p/>
 * размеры экрана:
 * @see java.awt.Dimension
 * @see java.awt.Toolkit#getScreenSize()
 * @see java.awt.Toolkit#getDefaultToolkit()
 */
public class RandomLocatedFrame {
}
