package term04.m02d04;

/**
 * Требуется реализовать графическое приложение "Калькулятор".
 * <p/>
 * Калькулятор должен поддерживать элементарные арифметические операции над рациональными числами:
 * 1) сложение;
 * 2) вычитание;
 * 3) умножение;
 * 4) деление.
 *
 * @author adkozlov
 * @see java.awt.GridLayout#GridLayout(int, int)
 * @see javax.swing.JTextArea
 */
public class JCalculator {
}
