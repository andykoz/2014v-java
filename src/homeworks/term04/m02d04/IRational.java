package term04.m02d04;

import java.math.BigInteger;

/**
 * Интерфейс, описывающий описывающией операции над рациональными числами.
 * <p/>
 * Дроби должны храниться в несократимом виде.
 *
 * @author adkozlov
 * @see java.math.BigInteger
 */
public interface IRational extends Comparable<IRational> {

    /**
     * Сложение.
     *
     * @param rational слагаемое
     * @return сумма
     */
    IRational add(IRational rational);

    /**
     * Вычитание.
     *
     * @param rational вычитаемое
     * @return разность
     */
    IRational substract(IRational rational);

    /**
     * Умножение.
     *
     * @param rational множитель
     * @return произведение
     */
    IRational multiply(IRational rational);

    /**
     * Деление.
     *
     * @param rational делитель
     * @return частное
     */
    IRational divide(IRational rational);

    /**
     * Числитель.
     *
     * @return числитель
     */
    BigInteger getNumerator();

    /**
     * Знаменатель.
     *
     * @return знаменатель
     */
    BigInteger getDenominator();

    /**
     * Проверка равенства дроби и объекта.
     * Для генерации метода рекомендуется использовать возможности среды программирования.
     *
     * @param o объект
     * @return равны ли объекты
     * @see Object#toString()
     */
    @Override
    boolean equals(Object o);

    /**
     * Значение хеш-кода числа.
     * Для генерации метода рекомендуется использовать возможности среды программирования.
     *
     * @return хеш-код
     * @see Object#toString()
     */
    @Override
    int hashCode();

    /**
     * Строковое представление дроби.
     * <p/>
     * Примеры:
     * 1) "5/3"
     * 2) "2" ("512/256", "2/1" -- неверное представление числа 2)
     *
     * @return строковое представление дроби.
     * @see Object#toString()
     */
    @Override
    String toString();
}
