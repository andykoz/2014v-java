package term03.integerCollections;

import term03.m09_d19.IntegerCollection;

/**
 * Интерфейс линейной структуры данных, хранящей целые числа.
 * <p/>
 * (см. http://docs.oracle.com/javase/6/docs/api/java/util/List.html)
 */
public interface IntegerList extends IntegerCollection {

    /**
     * Вернуть элемент, находящийся на указанной позиции.
     *
     * @param index номер позиции
     * @return искомый элемент
     */
    long get(int index);

    /**
     * Установить элемент, находящийся на указанной позиции, равным указанному значению.
     *
     * @param index   номер позиции
     * @param element новое значение
     * @return предыдущее значение на указанной позиции
     */
    long set(int index, long element);

    /**
     * Добавить элемент на указанную позицию (со сдвигом).
     *
     * @param index   номер позиции
     * @param element новое значение
     */
    void add(int index, long element);

    /**
     * Удалить элемент, находящийся на указанной позиции (со сдвигом).
     *
     * @param index номер позиции
     * @return предыдущее значение на указанной позиции
     */
    long remove(int index);
}
