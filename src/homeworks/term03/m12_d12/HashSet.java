package term03.m12_d12;

import term03.collections.Collection;
import term03.collections.Set;

import java.util.Iterator;

/**
 * Хеш-таблица (открытое хеширование).
 *
 * @param <E> тип элементов
 * @see java.util.HashSet
 */
public class HashSet<E> implements Set<E> {
    @Override
    public int size() {
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public boolean add(E e) {
        return false;
    }

    @Override
    public boolean addAll(Collection<E> c) {
        boolean result = false;

        for (Object o : c.toArray()) {
            result |= add((E) o);
        }

        return result;
    }

    @Override
    public boolean remove(E e) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<E> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<E> c) {
        return false;
    }

    @Override
    public boolean contains(E e) {
        return false;
    }

    @Override
    public boolean containsAll(Collection<E> c) {
        return false;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public Iterator<E> iterator() {
        return null;
    }

    @Override
    public String toString() {
        return null;
    }
}
