package term03.m12_d12;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

/**
 * Демонастрация разности работы дерева и хеш-таблицы.
 */
public class Difference {

    public static void main(String[] args) {
        Long l = new Long(2 * (long) Integer.MAX_VALUE);
        System.out.println(l + " " + l.hashCode()); // хеш-код для типа Long

        String s = "hello world";
        String t = "hello world"; // разница в одном символе, хеш-код изменяется не сильно
        String q = "hellp world"; // разница в одном символе, хеш-код изменяется сильно
        System.out.println(s.hashCode() + " " + t.hashCode() + " " + q.hashCode());

        Set<Long> hashSet = new HashSet<>();
        Set<Long> treeSet = new TreeSet<>();

        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            long value = random.nextLong() % 1000;
            hashSet.add(value);
            treeSet.add(value);
        }

        System.out.println(hashSet.toString()); // элементы отсортированы
        System.out.println(treeSet.toString()); // элементы не отсортированы
    }
}
