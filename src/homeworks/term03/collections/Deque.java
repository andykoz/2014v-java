package term03.collections;

/**
 * Структура данных "дек".
 *
 * @param <E> тип элементов, содержащихся в деке.
 * @see java.util.Deque
 */
public interface Deque<E> extends Queue<E> {

    /**
     * Положить элемент в начало дека.
     *
     * @param element элемент
     * @return добавлен ли элемент
     */
    boolean offerFirst(E element);

    /**
     * Удалить и вернуть элемент из начала дека.
     *
     * @return значение в начале дека или null, если дек пуст
     */
    E pollFirst();

    /**
     * Посмотреть элемент в начале дека.
     *
     * @return значение в начале дека или null, если дек пуст
     */
    E peekFirst();

    /**
     * Положить элемент в конец дека.
     *
     * @param element элемент
     * @return добавлен ли элемент
     */
    boolean offerLast(E element);

    /**
     * Удалить и вернуть элемент из конца дека.
     *
     * @return значение в конце дека или null, если дек пуст
     */
    E pollLast();

    /**
     * Посмотреть элемент в конце дека.
     *
     * @return значение в конце дека или null, если дек пуст
     */
    E peekLast();
}