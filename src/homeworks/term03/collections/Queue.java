package term03.collections;

/**
 * Структура данных "очередь".
 *
 * @param <E> тип элементов, содержащихся в очереди.
 * @see java.util.Queue
 */
public interface Queue<E> extends Collection<E> {

    /**
     * Положить элемент в конец очереди.
     *
     * @param element элемент
     * @return добавлен ли элемент
     */
    boolean offer(E element);

    /**
     * Удалить и вернуть элемент из начала очереди.
     *
     * @return значение в начале очереди или null, если очередь пуста
     */
    E poll();

    /**
     * Посмотреть элемент в начале очереди.
     *
     * @return значение в начале очереди или null, если очередь пуста
     */
    E peek();
}