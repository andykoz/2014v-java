package term03.collections;

/**
 * Структура данных "множество".
 *
 * @param <E> тип элементов, хранящихся в множестве
 * @see java.util.Set
 */
public interface Set<E> extends Collection<E> {

}
