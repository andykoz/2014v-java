package term03.collections;

/**
 * Структура данных "упорядоченное множество".
 *
 * @param <E> тип элементов, хранящихся в множестве
 * @see java.util.SortedSet
 */
public interface SortedSet<E extends java.lang.Comparable<E>> extends Set<E> {

    /**
     * Минимальный элемент в множестве.
     *
     * @return минимум
     */
    E first();

    /**
     * Максимальный элемент в множестве.
     *
     * @return максимум
     */
    E last();

    /**
     * Множество элементов исходного множества, строго меньших правой границы.
     *
     * @param toElement правая граница
     * @return меньшие элементы
     */
    SortedSet<E> headSet(E toElement);

    /**
     * Множество элементов исходного множества, больше либо равных левой границы.
     *
     * @param fromElement левая граница
     * @return больше либо равные элементы
     */
    SortedSet<E> tailSet(E fromElement);

    /**
     * Множество элементов исходного множества, больше либо равных левой границе и строго меньших правой границы.
     *
     * @param fromElement левая граница
     * @param toElement   правая граница
     * @return элементы из заданного диапазона
     */
    SortedSet<E> subSet(E fromElement, E toElement);
}
