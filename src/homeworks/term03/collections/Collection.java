package term03.collections;

/**
 * Интерфейс абстрактной структуры данных.
 *
 * @param <E> тип элементов, содержащихся в структуре.
 * @see java.util.Collection
 */
public interface Collection<E> extends Iterable<E> {

    /**
     * Количество элементов, хранимых в коллекции.
     *
     * @return количество элементов
     */
    int size();

    /**
     * Проверка коллекции на пустоту.
     *
     * @return пуста ли коллекция
     */
    boolean isEmpty();

    /**
     * Очистить коллекцию.
     */
    void clear();

    /**
     * Добавить указанный элемент в коллекцию.
     *
     * @param e добавляемый элементы
     * @return добавлен ли элемент
     */
    boolean add(E e);

    /**
     * Добавить все элементы данной коллекции в текущую.
     *
     * @param c добавляемые элементы
     * @return добавлена ли элементы
     */
    boolean addAll(Collection<E> c);

    /**
     * Удалить одно вхождение указанного элемента из коллекции, если такое существует.
     *
     * @param e удаляемый элемент
     * @return удален ли элемент
     */
    boolean remove(E e);

    /**
     * Удалить все вхождения элементов данной коллекции из текущей.
     *
     * @param c удаляемые элементы
     * @return удалены ли элементы
     */
    boolean removeAll(Collection<E> c);

    /**
     * Оставить в текущей коллекции только элементы, указанные в данной коллекции.
     *
     * @param c оставляемые элементы
     * @return оставлены ли элементы
     */
    boolean retainAll(Collection<E> c);

    /**
     * Проверить, содержит ли коллекция указанный элемент.
     *
     * @param e элемент
     * @return содержит ли коллекция элемент
     */
    boolean contains(E e);

    /**
     * Проверить, содержит ли текущая коллекция элементы, указанные в данной коллекции.
     *
     * @param c элементы
     * @return содержит ли коллекция элементы
     */
    boolean containsAll(Collection<E> c);

    /**
     * Массив, содержащий элементы коллекции.
     *
     * @return массив элементов
     */
    Object[] toArray();

    /**
     * @param o объект
     * @return равны ли все элементы
     * @see Object
     */
    boolean equals(Object o);

    /**
     * @return строка, содержащая все элементы коллекции
     * @see Object
     */
    String toString();

    /**
     * @return значение хеш-функции
     * @see Object
     */
    int hashCode();
}