package term03.m11_d14;

import term03.collections.Collection;
import term03.collections.SortedSet;

import java.util.Arrays;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Дерево.
 *
 * @param <E> тип элементов
 * @see java.util.TreeSet
 */
public class TreeSet<E extends Comparable<E>> implements SortedSet<E> {

    public static String EXCEPTION_MESSAGE = "Set is empty";

    private int size = 0;
    private Node root = null;

    private class Node {
        E value;
        Node left, right;

        private Node(E value) {
            this.value = value;
        }

        @Override
        public String toString() {
            String result = "";

            if (left != null) {
                result += left.toString() + ", ";
            }

            result += value;

            if (right != null) {
                result += ", " + right.toString();
            }

            return result;
        }
    }

    @Override
    public E first() {
        if (isEmpty()) {
            throw new NoSuchElementException(EXCEPTION_MESSAGE);
        }

        Node current = root;
        while (current.left != null) {
            current = current.left;
        }

        return current.value;
    }

    @Override
    public E last() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public SortedSet<E> headSet(E toElement) {
        SortedSet<E> result = new TreeSet<>();

        for (E e : this) {
            if (e.compareTo(toElement) < 0) {
                result.add(e);
            }
        }

        return result;
    }

    @Override
    public SortedSet<E> tailSet(E fromElement) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public SortedSet<E> subSet(E fromElement, E toElement) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return root == null;
    }

    @Override
    public void clear() {
        root = null;
        size = 0;
    }

    @Override
    public boolean add(E e) {
        if (isEmpty()) {
            root = new Node(e);
            size++;
            return true;
        }

        Node current = root;
        Node parent = null;

        while (current != null) {
            parent = current;

            if (e.compareTo(current.value) < 0) {
                current = current.left;
            } else if (e.compareTo(current.value) > 0) {
                current = current.right;
            } else {
                return false;
            }
        }

        Node temp = new Node(e);
        size++;
        if (e.compareTo(parent.value) < 0) {
            parent.left = temp;
        } else {
            parent.right = temp;
        }

        return true;
    }

    @Override
    public boolean addAll(Collection<E> c) {
        boolean result = false;

        for (E e : c) {
            result |= add(e);
        }

        return result;
    }

    @Override
    public boolean remove(E e) {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean removeAll(Collection<E> c) {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean retainAll(Collection<E> c) {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean contains(E e) {
        Node current = root;

        while (current != null) {
            int delta = e.compareTo(current.value);

            if (delta < 0) {
                current = current.left;
            } else if (delta > 0) {
                current = current.right;
            } else {
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean containsAll(Collection<E> c) {
        for (E e : c) {
            if (!contains(e)) {
                return false;
            }
        }

        return true;
    }

    @Override
    public Object[] toArray() {
        Object[] result = new Object[size()];

        int i = 0;
        for (Iterator<E> iterator = iterator(); iterator.hasNext(); i++) {
            result[i] = iterator.next();
        }

        return result;
    }

    @Override
    public Iterator<E> iterator() {
        return new TreeSetIterator();
    }

    private class TreeSetIterator implements Iterator<E> {

        private Node current;

        private TreeSetIterator() {
            current = root;
        }

        @Override
        public boolean hasNext() {
            return current != null;
        }

        @Override
        public E next() {
            E result = current.value;

            current = current.right;

            return result;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    @Override
    public String toString() {
        return "[" + root.toString() + "]";

        //return Arrays.toString(toArray());
    }
}
