package term03.m11_d14;

import java.util.Iterator;

/**
 * @author adkozlov
 */
public class Tester {

    public static void main(String[] args) {
        TreeSet<Integer> set = new TreeSet<Integer>();

        set.add(5);
        set.add(3);
        set.add(6);
        set.add(8);
        set.add(7);

        System.out.println(set);

        for (Iterator<Integer> iterator = set.iterator(); iterator.hasNext(); ) {
            System.out.println(iterator.next());
        }
    }
}
