package term03.m10_d03;

import term03.integerCollections.IntegerStack;

/**
 * Структура данных "стек", хранящая целые числа (реализация на списке).
 * (http://docs.oracle.com/javase/6/docs/api/java/util/Stack.html)
 */
public class IntegerStackListImpl implements IntegerStack {
    @Override
    public boolean push(long element) {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public long pop() {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public long peek() {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public int size() {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean isEmpty() {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void clear() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean add(long element) {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean remove(long element) {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean contains(long element) {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
