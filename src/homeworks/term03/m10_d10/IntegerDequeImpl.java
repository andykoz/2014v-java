package term03.m10_d10;

import term03.integerCollections.IntegerDeque;

/**
 * Структура данных "дек", хранящая целые числа (реализация на списке).
 * (http://docs.oracle.com/javase/6/docs/api/java/util/Deque.html)
 */
public class IntegerDequeImpl extends IntegerQueueImpl implements IntegerDeque {

    private class DequeNode extends Node {
        public Node prev;

        public DequeNode(long value, Node next, Node prev) {
            super(value, next);
            this.prev = prev;
        }
    }

    @Override
    public boolean offerFirst(long element) {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public long pollFirst() {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public long peekFirst() {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean offerLast(long element) {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public long pollLast() {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public long peekLast() {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
