package term03.m10_d10;

import term03.integerCollections.IntegerQueue;

/**
 * Структура данных "очередь", хранящая целые числа (реализация на списке).
 * (http://docs.oracle.com/javase/6/docs/api/java/util/Queue.html)
 */
public class IntegerQueueImpl implements IntegerQueue {

    protected Node head, tail;
    protected int size;

    protected class Node {
        public long value;
        public Node next;

        public Node(long value, Node next) {
            this.value = value;
            this.next = next;
        }
    }

    @Override
    public boolean offer(long element) {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public long poll() {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public long peek() {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public int size() {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean isEmpty() {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void clear() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean add(long element) {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean remove(long element) {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean contains(long element) {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
