package term03.m09_d19;

import java.util.NoSuchElementException;

/**
 * Структура данных "вектор", хранящая целые числа.
 */
public class IntegerVector implements IntegerList {

    private long[] elements = new long[10];
    private int size = 0;

    @Override
    public long get(int index) {
        if (0 <= index && index < size()) {
            return elements[index];
        } else {
            throw new NoSuchElementException();
        }
    }

    @Override
    public long set(int index, long element) {
        if (0 <= index && index < size()) {
            long result = elements[index];

            elements[index] = element;

            return result;
        } else {
            throw new NoSuchElementException();
        }
    }

    @Override
    public void add(int index, long element) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public long remove(int index) {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public void clear() {
        elements = new long[10];
        size = 0;
    }

    @Override
    public boolean add(long element) {
        ensureCapacity();

        elements[size++] = element;

        return true;
    }

    private void ensureCapacity() {
    }

    @Override
    public boolean remove(long element) {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean contains(long element) {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
