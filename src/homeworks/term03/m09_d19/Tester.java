package term03.m09_d19;

public class Tester {

    public static void main(String[] args) {
        IntegerVector vector = new IntegerVector();

        vector.add(1);
        vector.add(2);
        vector.add(3);

        System.out.println(vector.size());

        vector.clear();
        System.out.println(vector.isEmpty());
    }
}
