package term03.m12_d26;

import java.util.ArrayList;
import java.util.List;

/**
 * Для реализации данного задания полезно вспомнить следующие методы класса
 *
 * @author adkozlov
 * @see java.lang.String:
 * @see String#substring(int)
 * @see String#substring(int, int)
 * @see String#length()
 * @see String#charAt(int)
 */
public class StringMatching {

    /**
     * Требуется вычислить полиномиальный хеш заданной строки.
     *
     * @param s строка
     * @return значение полиномиального хеша
     */
    private long polynomialHash(String s) {
        return 0;
    }

    /**
     * Требуется реализовать алгоритм поиск наибольшей подстроки двух строк.
     *
     * @param s первая строка
     * @param t вторая строка
     * @return наибольшая общая подстрока (если строки не пересекаются, строка должна быть пустой)
     */
    private String longestCommonSubstring(String s, String t) {
        return "";
    }

    /**
     * Требуется реализовать алгоритм поиска подстроки в строки.
     *
     * @param pattern образец
     * @param text    текст, внутри которого производится поиск образца
     * @return список позиций, на которых строка pattern встречается в text (если текст не содержит образец, список должен быть пуст)
     */
    private List<Integer> rabinKarp(String pattern, String text) {
        List<Integer> result = new ArrayList<Integer>();

        return result;
    }

    /**
     * Требуется считать 2 строки с экрана.
     * И вывести результаты для обоих алгоритмов.
     *
     * @param args
     */
    public static void main(String[] args) {

    }
}
