package term02.m01_d26;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

/*
Требуется реализовать урезанный аналог функции split(String regex) из класса String.

С экрана вводится строка s, слова в которой разделены одиночными пробелами.
Функция split(String s) должна возвращать массив, каждый элемент которого -- слово из строки.

Пример:
input: "Мама мыла раму."
output: ["Мама", "мыла", "раму."]

Задачу стоит показывать только если на экран выводится строка "accepted".

Вам потребуются следующие функции класса String:
int length();
char charAt(int index);
substring(int beginIndex);
substring(int beginIndex, int endIndex).

Документацию можно прочитать здесь:
http://docs.oracle.com/javase/6/docs/api/java/lang/String.html
 */

public class Split {

    public static void main(String[] args) throws IOException {
        BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
        String s = bf.readLine();

        System.out.println(Arrays.deepEquals(s.split(" "), splitByBlank(s)) ? AC_MESSAGE : WA_MESSAGE);
    }

    final private static String AC_MESSAGE = "accepted";
    final private static String WA_MESSAGE = "wrong answer";
    final private static char BLANK = ' ';

    private static String[] splitByBlank(String s) {
        return null;
    }
}
