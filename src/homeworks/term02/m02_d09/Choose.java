package term02.m02_d09;

/*
На вход заданы два неотрицательных целых числа n и k. Требуется рекурсивно вычислить число сочетаний из n по k.

input:
5 3

output:
10

 */

import java.util.Scanner;

public class Choose {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println(choose(sc.nextInt(), sc.nextInt()));
    }

    private static int choose(int n, int k) {
        return 0;
    }
}
