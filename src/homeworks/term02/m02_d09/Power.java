package term02.m02_d09;

import java.util.Scanner;

/*
На вход задано неотрицательное целое число n. Требуется рекурсивно определить, является ли оно степенью двойки.

input:
65536

output:
yes

 */

public class Power {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println(isPowerOf2(sc.nextInt()) ? "yes" : "no");
    }

    private static boolean isPowerOf2(int n) {
        return false;
    }
}
