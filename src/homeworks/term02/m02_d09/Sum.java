package term02.m02_d09;

import java.util.Scanner;

/*
На вход задано целое число n. Требуется рекурсивно посчитать сумму его цифр.

input:
-179

output:
17

 */

public class Sum {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println(sumOfDigits(sc.nextInt()));
    }

    private static int sumOfDigits(int n) {
        return 0;
    }

}
