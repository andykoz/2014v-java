package term02.m02_d09;

/*
На вход задана строка. Требуется рекурсивно проверить, что строка является палиндромом.

input:
radar

output:
yes
 */

import java.util.Scanner;

public class Palindrome {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println(isPalindrome(sc.nextLine()) ? "yes" : "no");
    }

    private static boolean isPalindrome(String s) {
        return false;
    }
}
