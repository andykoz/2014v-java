package term02.m02_d02;

import java.util.Scanner;

/*
Заданы две строчки text и pattern.
Требуется опеределить:
1) является ли строка pattern началом строки text (startWith);
2) является ли строка pattern концом строки text (endsWith).

Один из вариантов написания:
1) проверить, что длина строки text не меньше длины строки pattern;
2) посимвольно сравнить строку pattern с соответствующим концом строки text.

input:
Prefix is a beginning of a string.
Prefix

output:
String "Prefix is a beginning of a string." starts with the string "Prefix".
String "Prefix is a beginning of a string." doesn't end with the string "Prefix".
 */

public class Substring {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String text = sc.nextLine();
        String pattern = sc.nextLine();

        System.out.printf(FORMAT_STRING, text, startsWith(text, pattern) ? START_POSITIVE_FORM : START_NEGATIVE_FORM, pattern);
        System.out.printf(FORMAT_STRING, text, endsWith(text, pattern) ? END_POSITIVE_FORM : ENDS_NEGATIVE_FORM, pattern);
    }

    final private static String FORMAT_STRING = "String \"%s\" %s with the string \"%s\".\n";
    final private static String START_POSITIVE_FORM = "starts";
    final private static String START_NEGATIVE_FORM = "doesn't start";
    final private static String END_POSITIVE_FORM = "ends";
    final private static String ENDS_NEGATIVE_FORM = "doesn't end";

    private static boolean startsWith(String s, String prefix) {
        return false;
    }

    private static boolean endsWith(String s, String suffix) {
        return false;
    }
}
