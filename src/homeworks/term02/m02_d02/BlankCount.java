package term02.m02_d02;

import java.util.Scanner;

/*
Задана строка s. Требуется посчитать количество символов BLANK в этой строке.

input:
for (int i = INTEGER.MIN_VALUE; i < Integer.MAX_VALUE; i++);

output:
8
 */

public class BlankCount {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println(blankCount(sc.nextLine()));
    }

    final private static char BLANK = ' ';

    private static int blankCount(String s) {
        return 0;
    }
}
