package term02.m03_d02;

/*
В файле array.txt задан массив целых чисел в формате из задания Generator.
Требуется реализовать три сортировки.
Считывание массива должно происходить в функции readArray().
 */

import java.util.Arrays;

public class Sorts {

    public static void main(String[] args) {
        int[] array = readArray();

        int[] select = selectionSort(array);
        int[] bubble = bubbleSort(array);
        int[] insertion = insertionSort(array);

        Arrays.sort(array);
        printlnVerdict(array, select, SORTS[0]);
        printlnVerdict(array, bubble, SORTS[1]);
        printlnVerdict(array, insertion, SORTS[2]);
    }

    final private static String FORMAT = "%s sort: %s\n";
    final private static String CORRECT = "correct";
    final private static String INCORRECT = "incorrect";
    final private static String[] SORTS = {"Selection", "Bubble", "Insertion"};

    private static void printlnVerdict(int[] pattern, int[] candidate, String name) {
        System.out.printf(FORMAT, name, Arrays.equals(pattern, candidate) ? CORRECT : INCORRECT);
    }

    private static int[] readArray() {
        return new int[0];
    }

    private static int[] selectionSort(int[] array) {
        return null;
    }

    private static int[] bubbleSort(int[] array) {
        return null;
    }

    private static int[] insertionSort(int[] array) {
        return null;
    }
}
