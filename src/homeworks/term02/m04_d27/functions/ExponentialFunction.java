package term02.m04_d27.functions;

/*
Показательная функция вида:
f(x) = c * b^x.
 */

public class ExponentialFunction implements Function {

    final private double coefficient, base;

    public ExponentialFunction(double coefficient, double base) {
        this.coefficient = coefficient;
        this.base = base;
    }

    public ExponentialFunction() {
        coefficient = 1;
        base = Math.E;
    }

    @Override
    public double getValue(double argument) {
        return coefficient * Math.pow(base, argument);
    }

    @Override
    public Function getDerivative() {
        return new ExponentialFunction(coefficient * Math.log(base), base);
    }
}
