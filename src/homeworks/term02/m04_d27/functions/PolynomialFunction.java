package term02.m04_d27.functions;

import java.util.Random;

/*
Полиномиальная функция вида:
f(x) = c_0 * x^0 + c_1 * x^1 + ... + c_n * x^n.
 */

public class PolynomialFunction implements Function {

    final private double[] coefficients;

    public int getDegree() {
        return coefficients.length - 1;
    }

    public PolynomialFunction(double[] coefficients) {
        this.coefficients = coefficients;
    }

    public PolynomialFunction(int degree) {
        coefficients = new double[degree + 1];

        Random random = new Random();
        for (int i = 0; i < coefficients.length; i++) {
            coefficients[i] = random.nextDouble() * random.nextInt() * (random.nextBoolean() ? 1 : -1);
        }
    }

    @Override
    public double getValue(double argument) {
        double result = 0;

        double power = 1;
        for (double coefficient : coefficients) {
            result += coefficient * power;
            power *= argument;
        }

        return result;
    }

    @Override
    public Function getDerivative() {
        if (getDegree() > 0) {
            double[] newCoefficients = new double[getDegree()];
            for (int i = 0; i < newCoefficients.length; i++) {
                newCoefficients[i] = (i + 1) * coefficients[i + 1];
            }

            return new PolynomialFunction(newCoefficients);
        } else {
            return new PolynomialFunction(new double[]{0});
        }
    }
}
