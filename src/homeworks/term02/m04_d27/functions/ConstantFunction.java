package term02.m04_d27.functions;

/*
Константная функция вида:
f(x) = c.
 */

public class ConstantFunction implements Function {

    final private double value;

    public ConstantFunction(double value) {
        this.value = value;
    }

    public ConstantFunction() {
        value = 0;
    }

    @Override
    public double getValue(double argument) {
        return value;
    }

    @Override
    public Function getDerivative() {
        return new ConstantFunction();
    }
}
