package term02.m04_d27;

/*

27 апреля 2013
Самостоятельная работа

Требуется реализовать интерполяционный поиск.
Тело процедуры void main(String[] args) изменять запрещено.
Также следует избегать дублирования кода.

Функция long[] generateArray(int length) должна выдавать массив длины length, заполненный случайными целыми числами.

Функция long[] generateSortedArray(int length) должна выдавать отсортированный массив длины length, заполненный случайными целыми числами.
Для сортировки массива рекомендуется использовать встроенную процедуру:
void Arrays.sort(long[] a).

Функция int search(long[] array, long query) должна выдавать:
1) целое число из полуинтервала [0, array.length), если число query содержится в массиве array;
2) -1 в противном случае.

Задача считается выполненной и оценивается из 10 баллов, если выводится сообщение "succeeded".
В противном случае, задача считается невыполненной и оценивается из 3 баллов.
 */

import java.util.Arrays;

public class InterpolationSearch {

    public static void main(String[] args) {
        boolean flag = true;

        long[] array = generateSortedArray(1 << 16);
        long[] queries = generateArray(1 << 8);

        for (long query : queries) {
            flag &= Arrays.binarySearch(array, query) == search(array, query);
        }

        if (flag) {
            System.out.println("succeeded");
        } else {
            System.err.println("failed");
        }
    }

    private static long[] generateArray(int length) {
        return null;
    }

    private static long[] generateSortedArray(int length) {
        return null;
    }

    private static int search(long[] array, long query) {
        return Arrays.binarySearch(array, query);
    }
}
