package term02.m04_d27;

/*
Требуется реализовать:
1) поиск минимума заданной функции (троичный поиск);
2) поиск максимума заданной функции (поиск с помощью золотого сечения).
 */

import term02.m04_d27.functions.Function;

public class ExtremumSearch {

    public static void main(String[] args) {

    }

    private static final double phi = (1 + Math.sqrt(5)) / 2;

    public static double ternarySearchMin(Function function, double left, double right) {
        return 0;
    }

    private static double goldenSectionSearchMax(Function function, double left, double right) {
        return 0;
    }
}
